module PlayerTest where

import           Cards
import           Control.Monad.Reader
import           Data.Function
import           Hearts.Types
import           Player
import           Player               as P
import           Test.Hspec
import           Test.QuickCheck


isLength5 :: [a] -> Maybe Bool
isLength5 [_,_,_,_,_] = Just True
isLength5 _           = Nothing

brokenState :: MyGameState
brokenState = MyGameState [] [] 3 True

unBrokenState :: MyGameState
unBrokenState = MyGameState [] [] 3 False

sampleTable :: Table
sampleTable = Table
  "1"
  [Card Club Four, Card Heart Three]
  [(Card Club Two, "2")]
  initialState

clubsTrick :: [Card] -> Table
clubsTrick cs = Table
  "1"
  cs
  [(Card Club Two, "2")]
  initialState

clubsTrickBroken :: [Card] -> Table
clubsTrickBroken cs = Table
  "1"
  cs
  [(Card Club Two, "2")]
  brokenState

allHearts :: Table
allHearts = Table
  "1"
  [Card Heart Four, Card Heart Three, Card Spade Queen]
  [(Card Club Two, "2")]
  initialState

clubTrick = [(Card Club Two, "2")]
noHeartsHand = [Card Heart Four, Card Heart Three, Card Spade Queen]
allHand = [Card Heart Seven, Card Club Three, Card Diamond King, Card Spade Ten]

instance Arbitrary Suit where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary Rank where
  arbitrary = arbitraryBoundedEnum

instance Arbitrary Card where
  arbitrary = Card <$> arbitrary <*> arbitrary

arbTrick :: Gen [(Card, PlayerId)]
arbTrick = listOf $ do
  a <- arbitrary
  b <- posNum
  return (a, show b)

instance Arbitrary MyGameState where
  arbitrary = MyGameState <$>
    listOf arbTrick <*>
    arbitrary <*>
    posNum <*>
    arbitrary

posNum :: Gen Int
posNum = sized $ \s -> return s

tests :: IO ()
tests = hspec $ do
  -- describe "multiFiltering" $ do
  --   context "Maybe type" $ do
  --     it "filters based on multiple number limiting funcs" $ property $
  --       \x y z -> (multiFiltering ((pure .) <$> [(>x), (<y), (==0) . flip mod 2]) [1..z] :: Maybe [Int])
  --                 == pure [a | a <- [1..z], a > x, a < y, a `mod` 2 == 0]
  --     it "returns no values if a Nothing is generated, even if something passes" $
  --       (multiFiltering [isLength5, Just . all (<9)] [[1..3], [1..5], [1..9], [1..20]] :: Maybe [[Int]])
  --         `shouldBe` Nothing


  describe "bestWhile" $ do
    it "finds the highest value which meets a condition" $
      (bestWhile ((==0) . flip mod 7) [1..200] :: Maybe Int)
      `shouldBe` (Just . maximum $ filter ((==0) . flip mod 7) [1..200])

  describe "validCards" $ do
    context "Following clubs suit" $ do
      it "must play clubs after a club, if a club is in the hand" $
        runReaderT validCards sampleTable
          `shouldBe` Just [Card Club Four]

      it "can play anything if no clubs" $
        let table = clubsTrickBroken [Card Heart Two, Card Spade King, Card Diamond Ace]
        in runReaderT validCards table
             `shouldBe` Just (hand table)

    context "Playing trumps" $
      let table = clubsTrick [Card Heart Two, Card Spade King, Card Diamond Ace]
          brokenTable = clubsTrickBroken [Card Heart Two, Card Spade King, Card Diamond Ace]
      in do
        it "must on turn one if hand is all trumps" $
          runReaderT validCards allHearts
           `shouldBe` Just (hand allHearts)

        it "can't on turn one if there's anything else in hand" $
          runReaderT validCards table
            `shouldBe` Just (filter ((/=Heart) . suit) $ hand table)

        it "can't if not broken" $ property $
          \s -> let minusHand = filter ((/=s) . suit) allHand
                    theTrick = [(Card s Three, "2")]
                in runReaderT validCards (Table "1" minusHand theTrick unBrokenState)
                   `shouldBe` Just (filter (not . isTrump) minusHand)

        it "can if broken and none of lead suit" $
          runReaderT validCards brokenTable
            `shouldBe` Just (hand brokenTable)

  describe "Reading/Showing MyGameState" $
    it "should be reversible" $ property $
      \s -> Just s == readState (show s)
