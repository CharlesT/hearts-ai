import           Control.Monad
import           System.Exit

import           Cards
import           Deck
import           EitherIO
import           Game          (newPlayer)
import           Hearts.Play   (playGame)
import           Hearts.Types
import           Logs          (writeGame)

import qualified Player

-- | Test a game of the Player against itself.
test_one_play :: [Player] -> IO Bool
test_one_play players = do
  played <- runEitherIO $ playGame 30000 players
  case played of
    Right gr -> writeGame gr >> return True
    -- Beware, you need to capture the error if run headless
    Left e   -> putStrLn "" >> print e >> return False

main :: IO ()
main = do
  played <- test_one_play $ map make_player ["a", "b", "c", "d"]
  if played
     then exitSuccess
     else exitFailure
  where
    make_player name = newPlayer name Player.playCard Player.makeBid
    -- TODO Add third player
