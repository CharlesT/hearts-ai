-- | Overall, my strategy is rather simple, to be honest. I haven't opted
--   for any kind of sophisticated AI which determines the best choice
--   methodically, but instead have opted for a series of heuristics.
--
--   First, I apply all the rules of the game to my hand, eliminating cards
--   I cannot play, as this makes them pointless to consider (see
--   'validCards' for the series of rules and how they combine). I then
--   take these cards and apply heuristics to them which change their
--   assigned data value ('Choice') such that the lower it gets, the more
--   likely the card is to be played. Once the heuristics have finished,
--   I play the card with the lowest 'Choice'.
--
--   Design patterns applied include:
--   * The ReaderT monad transformer, so that practically throughout the
--     entire program, the current instance of the 'Table' data type is
--     accessible, allowing easy queries with the knowledge that it will
--     not change (unlike StateT). It is used with Maybe, to make it
--     easier to indicate a failure of rule application.
--   * The above has allowed me to state rules in a rather declarative
--     fashion - again, see 'validCards'
--   * The State monad, to contain the 'Choice' values as they are
--     continuously modified. Pretty standard.
--   * A Parser Combinator library, to parse the 'MyGameState' data type.
{-# LANGUAGE DeriveFunctor  #-}
{-# LANGUAGE LambdaCase     #-}
{-# LANGUAGE NamedFieldPuns #-}
module Player where

-- You can add more imports as you need them.
import           Cards
import           Control.Applicative          (liftA2, liftA3, (<|>))
import           Control.Monad                (join, unless, when)
import           Control.Monad.Reader         (ReaderT (..), ask, asks, lift,
                                               mapReaderT)
import qualified Control.Monad.State.Strict   as S
import           Data.Char                    (isAlpha, isDigit)
import           Data.Foldable                (foldl')
import           Data.Function                (on)
import           Data.Functor.Identity
import           Data.List
import qualified Data.Map.Strict              as M
import           Data.Maybe                   (fromMaybe)
import qualified Data.Set                     as S
import qualified Data.Tree                    as T
import           Debug.Trace
import           Hearts.Types                 (BidFunc, PlayFunc, PlayerId)
import           Text.ParserCombinators.ReadP
import           Text.Read                    (readMaybe)
-- import Data.Map



------------------------------------
-- MAIN TYPES
------------------------------------

type Trick = [(Card, PlayerId)]

-- | Information passed by this player to its future self. Carries
-- info about previous rounds for better heuristics.
data MyGameState = MyGameState {
  tricks :: [Trick],
  played :: [Card], -- Cards previously played
  turn   :: Int, -- Current turn number (start at 0)
  broken :: Bool -- Have hearts been broken yet?
  }
  deriving (Eq, Ord, Show)

-- | All necessary info (including the player's state) about the
-- game as it is currently.
data Table = Table {
  playerId  :: PlayerId,
  hand      :: [Card],
  trick     :: [(Card, PlayerId)],
  following :: Bool,
  state     :: MyGameState
                   }

---------------------------------------------------
-- HANDLING STATE
---------------------------------------------------

-- | Take the relevant new information about the game and use it to
-- update the current state.
updateState :: [(Card, PlayerId)] -> Card -> MyGameState -> MyGameState
updateState t c s = let tri = t : tricks s
                        car = c : played s
                        tur = turn s + 1
                        bre = broken s || any isHeart (fst <$> t)
                    in MyGameState tri car tur bre

-- | Parse a string representation of a 'MyGameState'.
readState :: String -> Maybe MyGameState
readState = readParser parseState

---------------------------------------------------
-- PARSING STATE
---------------------------------------------------

-- | Take a parser and apply it to a string.
readParser :: ReadP a -> String -> Maybe a
readParser p = headMay . fmap fst . readP_to_S p

-- | Create a parser for MyGameState.
parseState :: ReadP MyGameState
parseState = (string "MyGameState " >>) $ between (char '{') (char '}') $ do
  tri <- string "tricks = " >> parseList parseTrick
  skipComma
  pla <- string "played = " >> parseList parseCard
  skipComma
  tur <- string "turn = " >> option 0 parseNum
  skipComma
  bro <- string "broken = " >> option False parseBool
  return $ MyGameState tri pla tur bro

-- | Parse a bool by only checking if it says 'True'.
parseBool :: ReadP Bool
parseBool = (=="True") <$> (string "True" <++ string "False")

-- | Parse a sequence of digits/alphabetic characters.
parsePlayerId :: ReadP String
parsePlayerId = many (satisfy (\a -> isDigit a || isAlpha a))

-- | Parse an arbitrary length integer number.
parseNum :: ReadP Int
parseNum = pRead $ many (satisfy isDigit)

-- | Parse an arbitrary length list of values recognised by the provided parser.
parseList :: ReadP a -> ReadP [a]
parseList pa = between (char '[') (char ']') (sepBy pa (char ','))

-- | A separator for sequenced values; takes and ignores a comma
-- and any spaces after.
skipComma :: ReadP ()
skipComma = char ',' >> skipSpaces

-- | Parse an instance of the 'Trick' type.
parseTrick :: ReadP Trick
parseTrick = parseList parsePlay

-- | Parse a single play from a trick.
parsePlay :: ReadP (Card, PlayerId)
parsePlay =
  between (char '(') (char ')') $ do
    car <- parseCard
    _ <- char ','
    pla <- between (char '"') (char '"') parsePlayerId
    return (car, pla)

-- | Parse an instance of the 'Card' type.
parseCard :: ReadP Card
parseCard = pRead (count 3 get) <++ pRead (count 2 get)

-- | Try to convert a string to the required datatype; if it doesn't
-- work, the parser will fail instead of raising an error.
pRead :: Read a => ReadP String -> ReadP a
pRead = (maybe pfail return . readMaybe =<<)



-- OTHER DATA TYPES




-------------------------------------
-- UTILITY FUNCTIONS
-------------------------------------

suit :: Card -> Suit
suit (Card s _) = s

rank :: Card -> Rank
rank (Card _ r) = r

spadeQ :: Card
spadeQ = Card Spade Queen

isHeart :: Card -> Bool
isHeart = (==Heart) . suit

isTrump :: Card -> Bool
isTrump c = case c of
  (Card Heart _)     -> True
  (Card Spade Queen) -> True
  _                  -> False

-- | Find the highest valued value in a subset of a given list.
bestWhile :: (Ord a) => (a -> Bool) -> [a] -> Maybe a
bestWhile = (maximumMay .) . filter

-- | Safe version of maximum.
maximumMay :: (Ord a) => [a] -> Maybe a
maximumMay [] = Nothing
maximumMay xs = pure $ maximum xs

-- | Safe version of minimum.
minimumMay :: (Ord a) => [a] -> Maybe a
minimumMay [] = Nothing
minimumMay xs = pure $ minimum xs

-- | Safe version of head.
headMay :: [a] -> Maybe a
headMay []    = Nothing
headMay (x:_) = Just x

-- | Safe version of tail.
tailMay :: [a] -> Maybe a
tailMay []     = Nothing
tailMay [x]    = Just x
tailMay (_:xs) = tailMay xs

-- | Find the card with the highest value in a given suit with a deck.
highestInSuit :: Suit -> [Card] -> Maybe Card
highestInSuit s = bestInSuitUnderMax (Card s maxBound)

-- | Only check the condition if the suit matches.
conditionUnderSuit :: Suit -> (Card -> Bool) -> Card -> Bool
conditionUnderSuit s f c = suit c == s && f c

-- | Highest (and therefore worst) card which won't win the trick
bestInSuitUnderMax :: Card -> [Card] -> Maybe Card
bestInSuitUnderMax c = bestWhile $ conditionUnderSuit (suit c) (<=c)

-- | Find the card which so far is winning the trick.
trickWinner :: Trick -> Maybe Card
trickWinner t = do
  s <- leadingSuit t
  highestInSuit s (fst <$> t)

-- | Get the suit which started the trick and therefore must be followed.
leadingSuit :: Trick -> Maybe Suit
leadingSuit = fmap suit . leadingCard

-- | Get the card which started the trick and therefore must be followed.
leadingCard :: Trick -> Maybe Card
leadingCard = tailMay . fmap fst

-- | See if a given hand needs to follow the initial suit
isFollowing :: Trick -> [Card] -> Bool
isFollowing t cs = fromMaybe False $ do
  lead <- leadingSuit t
  return $ elem lead $ fmap suit cs

-- | Checks if the current turn is the first.
isGameStart :: MyGameState -> Bool
isGameStart = (==0) . turn

-- | State to start the game with.
initialState :: MyGameState
initialState = MyGameState [] [] 0 False

-- | Similar to initialState, but assumes that the game is not at
-- the start.
basicState :: MyGameState
basicState = MyGameState [] [] 1 False

-------------------------------------------------
-- GAME CORE
-------------------------------------------------

-- | Take information about the current trick, and return a valid card
-- which (hopefully) will be help the Player win.
playCard :: PlayFunc
playCard myID myHand cards prev =
  let currState = case prev of
        -- This is an extra check JUST IN CASE the parser returns nothing for some reason,
        -- so that the game doesn't default to turn one and break the rules.
        Nothing -> initialState
        Just p -> fromMaybe basicState
                  (readState . snd $ p)

      lastTrick = maybe [] fst prev

      -- The state of the game, all information which can be accessed by
      -- any heuristics or rule-checking.
      currTable = Table
        myID
        myHand
        cards
        (isFollowing cards myHand)
        currState

      -- Run rule checking and heuristics. If Nothing is returned, then it should
      -- mean there's no matching cards so ideally discard high cards. Just a default.
      newCard = fromMaybe (maximum myHand) $
        runReaderT nextCard currTable -- impure

      -- Can't forget to update state for next time.
      newState = updateState lastTrick newCard currState

  in (newCard, show newState)





-- | Table data is always accessible, and return values are optional.
type Game = ReaderT Table Maybe

-- | Separate the application of the game's rules from the actual
-- decision-making about the optimal card choice.
nextCard :: Game Card
nextCard = validCards >>= selectionStrategy

-- | Take a hand of valid cards and decide which to use.
selectionStrategy :: [Card] -> Game Card
selectionStrategy = heuristicAnalysis

----------------------------------------------------------
-- RULES
----------------------------------------------------------


-- | Rules are simply a list of cards which they allow, obtained
-- through manipulating the 'hand' of the 'Table' data type.
type Rule = Game [Card]

-- | The first 'Rule' which does not contain Nothing is returned.
firstValidRule :: [Rule] -> Rule
firstValidRule = foldl' (<|>) (lift Nothing)

-- | Return the intersection of the hands generated by each rule.
overlapRules :: [Rule] -> Rule
overlapRules = foldl' ((<*>) . fmap intersect) (asks hand)

-- | Apply all known rules to the hand and return any cards which
-- can be played. Makes no decision about which card would be best.
validCards :: Game [Card]
validCards =
  firstValidRule [

    allTrumpsOnRoundOne,
    twoClubsOnRoundOne,
    allHeartsWithoutBreak,

    overlapRules [
        noPointsOnRoundOne,
        noHeartsWithoutBreak,
        followSuit
        ]
    ]


-- | Only works with 'firstValidRule'; 'overlapRules' would mean
-- overlapping Nothing, and thus returning Nothing.
ignoreRule :: Game a
ignoreRule = lift Nothing

-- | An extreme edge case. If your hand is all trumps, then any card
-- is playable.
allTrumpsOnRoundOne :: Game [Card]
allTrumpsOnRoundOne = do
  isFirstTrick <- asks $ isGameStart . state
  allTrumps <- asks $ all isTrump . hand
  if allTrumps && isFirstTrick
    then asks hand
    else ignoreRule

-- | 1/4 chance of occurring; if the player has the two of clubs, they
-- must play it on the first turn.
twoClubsOnRoundOne :: Game [Card]
twoClubsOnRoundOne = do
  firstToPlay <- asks $ null . trick
  isFirstTrick <- asks $ isGameStart . state
  hasTwoClubs <- asks $ elem (Card Club Two) . hand

  if firstToPlay && isFirstTrick && hasTwoClubs
    then return [Card Club Two]
    else ignoreRule

-- | It happens sometimes. Still need to play if so.
allHeartsWithoutBreak :: Game [Card]
allHeartsWithoutBreak = do
  allHearts <- asks $ all isHeart . hand
  notBroken <- asks $ not . broken . state

  if allHearts && notBroken
    then asks hand
    else ignoreRule


-- | Point cards cannot be played on the first turn.
noPointsOnRoundOne :: Game [Card]
noPointsOnRoundOne = do
  isFirstTrick <- asks $ isGameStart . state
  myHand <- asks hand

  return $ if isFirstTrick
    then filter (not . isTrump) myHand
    else myHand


-- | Cannot start a trick with a Heart unless Hearts have been broken.
noHeartsWithoutBreak :: Game [Card]
noHeartsWithoutBreak = do
  hasBroken <- asks $ broken . state
  noHearts <- asks $ filter (not . isHeart) . hand
  firstToPlay <- asks $ null . trick

  if not hasBroken && firstToPlay
    then return noHearts
    else asks hand


-- | If you have any of the lead suit, then they are the only cards
-- which can be played.
followSuit :: Game [Card]
followSuit = do
  leadSuit <- asks $ leadingSuit . trick
  myhand <- asks hand

  let matchingCards = do
        s <- leadSuit
        return $ filter ((==s) . suit) myhand

  let isNonZeroMatchingCards = maybe False (not . null) matchingCards

  if isNonZeroMatchingCards
    then lift matchingCards
    else asks hand



---------------------------------------------------------
-- STRATEGY
---------------------------------------------------------


-- | Provide initial data to the heuristics.
heuristicAnalysis :: [Card] -> Game Card
heuristicAnalysis cs = ReaderT $ \r ->
  S.evalState (runReaderT decisions r) $
  M.filterWithKey (const . flip elem cs) (startingChoices cs)


data Decision a =
    PlayASAP a
  | Neutral a
  | TryNotToPlay a
  | Discard a

  deriving (Eq, Show, Ord, Functor)

-- These instances are inane, yes, but useful. I haven't tested if
-- they pass the laws though.
instance Applicative Decision where
  pure = Neutral
  (<*>) (PlayASAP f) d     = f <$> d
  (<*>) (Neutral f) d      = f <$> d
  (<*>) (TryNotToPlay f) d = f <$> d
  (<*>) (Discard f) d      = f <$> d

instance Monad Decision where
  return = pure
  (>>=) (PlayASAP a) f     = f a
  (>>=) (Neutral a) f      = f a
  (>>=) (TryNotToPlay a) f = f a
  (>>=) (Discard a) f      = f a

type Choice = Decision Int
type Choices = M.Map Card Choice
type Strategy = ReaderT Table (S.State Choices)

setChoices :: (Choices -> Choices) -> Strategy ()
setChoices = S.modify

allCards :: [Card]
allCards = Card <$> [Spade ..] <*> [Two ..]

startingChoices :: [Card] -> Choices
startingChoices cs = M.fromSet go (S.fromList cs)
  where
    go :: Card -> Choice
    go c = case c of
      -- Card Spade Queen -> Discard 20
      Card Heart r -> Discard $ fromEnum (maxBound :: Rank) - fromEnum r
      Card _ r     -> Neutral $ fromEnum r


maxDecision :: Bounded a => Decision a
maxDecision = Discard maxBound

decisions :: Strategy (Maybe Card)
decisions = do
  preferCardsJustBelowCurrentLeader
  noSpadesIfQueenInPlay
  -- ifStartingPlayLow
  -- bleedSpadesIfHaveQueen
  playHighAtStart
  ifNotFollowingThenDiscard

  finalDecision

updateChoices :: (Card -> Choice -> Choice) -> Strategy ()
updateChoices f = setChoices $ M.mapWithKey f

flipPreference :: Choice -> Choice
flipPreference = fmap negate

preferIfBy :: (Card -> Bool) -> (Choice -> Choice) -> Strategy ()
preferIfBy f g = updateChoices $ \c -> if f c then g else id

preferCardsJustBelowCurrentLeader :: Strategy ()
preferCardsJustBelowCurrentLeader = do
  following <- asks following
  leadCard <- asks $ leadingCard . trick
  cards <- S.gets M.keys
  let idealCard = do
        c <- leadCard
        bestInSuitUnderMax c cards

  when following $
    preferIfBy ((==idealCard) . Just) (>>= PlayASAP)

noSpadesIfQueenInPlay :: Strategy ()
noSpadesIfQueenInPlay = do
  sqBeenPlayed <- asks $ any (elem spadeQ) . (fmap . fmap) fst . tricks . state
  haveSQ <- asks $ elem spadeQ . hand

  unless (sqBeenPlayed || haveSQ) $
    preferIfBy ((==Spade) . suit) (>>= Discard)


ifStartingPlayLow :: Strategy ()
ifStartingPlayLow = do
  startingTrick <- asks $ null . trick

  when startingTrick $
    preferIfBy ((<) Five . rank) ((subtract 20 <$>) . flipPreference)

bleedSpadesIfHaveQueen :: Strategy ()
bleedSpadesIfHaveQueen = do
  numSpades <- lift . S.gets $ length . filter ((==Spade) . suit) . M.keys

  preferIfBy (\c -> numSpades >= 4 && c == Card Spade Queen)
             (>>= PlayASAP . negate . flip div 2)

  when (numSpades <= 2) $
    preferIfBy (== Card Spade Queen) (>>= Discard)

playHighAtStart :: Strategy ()
playHighAtStart = do
  isEarly <- asks $ (<1) . turn . state
  when isEarly $
    preferIfBy ((>Nine) . rank) (subtract 20 <$>)

ifNotFollowingThenDiscard :: Strategy ()
ifNotFollowingThenDiscard = do
  following <- asks following
  unless following $
    preferIfBy (const True) (>>= Discard)

-- | Finally, pick either the best discard card, or the lowest valued card.
finalDecision :: Strategy (Maybe Card)
finalDecision = do
  choices <- lift S.get

  let foldingFunc = genFoldingFunc choices

  let defaultCard = minimumMay (M.keys choices)
  let final = fst $ M.foldlWithKey' foldingFunc (defaultCard, maxDecision) choices
  return final

genFoldingFunc :: M.Map Card Choice ->
                  (Maybe Card, Choice) -> Card -> Choice -> (Maybe Card, Choice)
genFoldingFunc m =
  let the_filter
        | any isDiscard (M.elems m) = (isDiscard, (>))
        | otherwise = (const True, (<))
  in genericFolder the_filter

genericFolder :: ((Choice -> Bool), (Choice -> Choice -> Bool)) ->
                 (Maybe Card, Choice) -> Card -> Choice -> (Maybe Card, Choice)
genericFolder (f, comp) (c1, choice1) c2 choice2 =
  if f choice2 && choice2 < choice1
  then (Just c2, choice2)
  else (c1, choice1)


isDiscard :: Choice -> Bool
isDiscard (Discard _) = True
isDiscard _           = False



-- | Not used, do not remove.
makeBid :: BidFunc
makeBid = undefined

