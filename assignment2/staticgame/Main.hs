module Main where

import Hearts.Play
import Hearts.Types
import Game
import EitherIO
import Logs
import safe qualified Player
import safe qualified Maximum
import safe qualified JustUnderMax
import Control.Monad

-- This sets up a tournament with four instances of your player playing against
-- each other.  You can run different players against each other, but you'll
-- need to change the Module names of those players (don't forget to change the
-- module name back to "Player" when you submit your code)
vsMax :: [Player]
vsMax = [
    newPlayer "Maximum" Maximum.playCard Maximum.makeBid
    , newPlayer "Player" Player.playCard Player.makeBid
    ]
        
vsJustUnder :: [Player]
vsJustUnder = [
    newPlayer "JustUnderMax" JustUnderMax.playCard JustUnderMax.makeBid
    , newPlayer "Player" Player.playCard Player.makeBid
    ]

playMatch :: [Player] -> IO ()
playMatch players = do
  clearAllLogs players
  played <- runEitherIO $ playGame 18000 players
  case played of
    Right gr@(GameResult hr scores _) -> do
      -- forM_ (reverse hr) print
      putStrLn "=============="
      forM_ scores print
      writeGame gr
    Left e -> print e
  

main :: IO ()
main = do
  putStrLn "+++++++++++++++"
  playMatch vsMax
  putStrLn "+++++++++++++++"
  playMatch vsJustUnder
  putStrLn "+++++++++++++++"
  
