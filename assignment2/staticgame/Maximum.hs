-- | Write a report describing your design and strategy here.
{-# LANGUAGE DeriveFunctor  #-}
{-# LANGUAGE NamedFieldPuns #-}
module Maximum where

-- You can add more imports as you need them.
import           Cards
import           Control.Applicative          (liftA2, liftA3, (<|>))
import           Control.Monad                (join)
import           Control.Monad.Reader         (ReaderT (..), ask, asks, lift,
                                               mapReaderT)
import qualified Control.Monad.State.Strict   as S
import           Data.Char                    (isAlpha, isDigit)
import           Data.Foldable                (foldl')
import           Data.Function                (on)
import           Data.Functor.Identity
import           Data.List
import           Data.Maybe                   (fromMaybe)
-- import           Debug.Trace
import qualified Data.Map.Strict              as M
import qualified Data.Set                     as S
import qualified Data.Tree                    as T
import           Hearts.Types                 (BidFunc, PlayFunc, PlayerId)
import           Text.ParserCombinators.ReadP
import           Text.Read                    (readMaybe)
-- import Data.Map



------------------------------------
-- NEW TYPES
------------------------------------

-- I know, this is an orphan instance, but as I am only allowed to modify
-- this file it is a necessary evil.
instance Bounded Card where
  minBound = Card Club Two
  maxBound = Card Heart Ace


type CardProb = (Card, Rational)
type Trick = [(Card, PlayerId)]


---------------------------------------------------
-- HANDLING STATE
---------------------------------------------------

data MyGameState = MyGameState {
  tricks :: [Trick],
  played :: [Card], -- Cards previously played
  turn   :: Int, -- Current turn number (start at 0)
  broken :: Bool -- Have hearts been broken yet?
  }
  deriving (Eq, Ord, Show)

numPlayers :: MyGameState -> Int
numPlayers = length . tricks

readParser :: ReadP a -> String -> Maybe a
readParser p = headMay . fmap fst . readP_to_S p

readState :: String -> Maybe MyGameState
readState = headMay . fmap fst . readP_to_S parseState

skipComma :: ReadP ()
skipComma = char ',' >> skipSpaces

parseState :: ReadP MyGameState
parseState = (string "MyGameState " >>) $ between (char '{') (char '}') $ do
  tri <- string "tricks = " >> parseList parseTrick
  skipComma
  pla <- string "played = " >> parseList parseCard
  skipComma
  tur <- string "turn = " >> option 0 parseNum
  skipComma
  bro <- string "broken = " >> option False parseBool
  return $ MyGameState tri pla tur bro

parseBool :: ReadP Bool
parseBool = (=="True") <$> (string "True" <++ string "False")

parsePlayerId :: ReadP String
parsePlayerId = many (satisfy (\a -> isDigit a || isAlpha a))

parseNum :: ReadP Int
parseNum = pRead $ many (satisfy isDigit)

parseList :: ReadP a -> ReadP [a]
parseList pa = between (char '[') (char ']') (sepBy pa (char ','))

parseTrick :: ReadP Trick
parseTrick = parseList parsePlay

parsePlay :: ReadP (Card, PlayerId)
parsePlay =
  between (char '(') (char ')') $ do
    car <- parseCard
    _ <- char ','
    pla <- between (char '"') (char '"') parsePlayerId
    return (car, pla)

parseCard :: ReadP Card
parseCard = pRead (count 3 get) <++ pRead (count 2 get)

pRead :: Read a => ReadP String -> ReadP a
pRead = (maybe pfail return . readMaybe =<<)


updateState :: [(Card, PlayerId)] -> Card -> MyGameState -> MyGameState
updateState t c s = let tri = t : tricks s
                        car = c : played s
                        tur = turn s + 1
                        bre = broken s || any isHeart (fst <$> t)
                    in MyGameState tri car tur bre






-- OTHER DATA TYPES

data Table = Table {
  playerId :: PlayerId,
  hand     :: [Card],
  trick    :: [(Card, PlayerId)],
  state    :: MyGameState
                   }

type Reader a = ReaderT a Identity
runReader :: Reader a c -> a -> c
runReader = (runIdentity .) . runReaderT

type Game = ReaderT Table Maybe


-------------------------------------
-- UTILITY FUNCTIONS
-------------------------------------
suit :: Card -> Suit
suit (Card s _) = s

rank :: Card -> Rank
rank (Card _ r) = r

initialState :: MyGameState
initialState = MyGameState [] [] 0 False

basicState :: MyGameState
basicState = MyGameState [] [] 1 False

-------------------------------------------------
-- GAME CORE
-------------------------------------------------

playCard :: PlayFunc
playCard myID myHand cards prev =
  let currState = case prev of
        -- This is an extra check JUST IN CASE the parser returns nothing for some reason,
        -- so that the game doesn't default to turn one and break the rules.
        Nothing -> initialState
        Just _ -> fromMaybe basicState
                  (readState . snd =<< prev)
      lastTrick = maybe [] fst prev

      currTable = Table
        myID
        myHand
        cards
        currState

      newMCard = runReaderT nextCard currTable
      newCard = fromMaybe (maximum myHand) newMCard -- impure

      newState = updateState lastTrick newCard currState

  in (newCard, show newState)



isHeart :: Card -> Bool
isHeart = (==Heart) . suit


-- | Take a list of monadic conditions, and return only the values
-- which pass all the conditions.
-- multiFiltering :: (Eq a, Monad m) => [a -> m Bool] -> [a] -> m [a]
-- multiFiltering fs = foldr (\x a -> do
--                               cond <- (fmap and . sequenceA) (fs <*> pure x)
--                               past <- a
--                               return $ if cond then x : past else past)
--                     (return [])


-- | Separate the application of the game's rules from the actual
-- decision-making about the optimal card choice.
nextCard :: Game Card
nextCard = validCards >>= selectionStrategy

---------------------------------------------------------
-- STRATEGY
---------------------------------------------------------


-- | Take a hand and decide which card to use.
selectionStrategy :: [Card] -> Game Card
selectionStrategy = lift . maximumMay

justUnderMax :: [Card] -> Game Card
justUnderMax cs = do
  highest_card <- asks $ trickWinner . trick

  let best = highest_card >>= flip bestInSuitUnderMax cs

  case best of
    Nothing -> lift $ headMay cs
    Just c  -> return c

heuristicAnalysis :: [Card] -> Game Card
heuristicAnalysis cs = ReaderT $ \r -> Just $
  S.evalState (runReaderT decisions r) $
  M.filterWithKey (const . flip elem cs) startingChoices

-- data CardProp a = C {card :: Card, prop :: a}
--   deriving (Eq, Show, Ord)

-- instance Functor CardProp where
--   fmap f cp = C (card cp) (f $ prop cp)

-- instance Applicative CardProp where
--   pure = C (Card Club Two)
--   (<*>) cf ca = prop cf <$> ca

-- type Choice = CardProp Rational


data Decision a =
    Discard a
  | StartOfTrick a
  | NoStrat a
  | LateGame a

  deriving (Eq, Show, Ord, Functor)

type Choice = Decision Int
type Choices = M.Map Card Choice
type Strategy = ReaderT Table (S.State Choices) Card

startingChoices :: Choices
startingChoices = M.fromSet go (S.fromList (Card <$> [Spade ..] <*> [Two ..]))
  where
    go :: Card -> Choice
    go c = case c of
      Card Spade Queen -> Discard 20
      Card Heart r     -> Discard $ fromEnum (maxBound :: Rank) - fromEnum r
      Card _ r         -> NoStrat $ fromEnum r


maxDecision :: Bounded a => Decision a
maxDecision = LateGame maxBound

decisions :: Strategy
decisions = finalDecision

finalDecision :: Strategy
finalDecision = do
  choices <- lift S.get
  defaultCard <- maximum <$> asks hand
  return $ fst $ M.foldlWithKey' go (defaultCard, maxDecision) choices
    where
      go :: (Card, Choice) -> Card -> Choice -> (Card, Choice)
      go (c1, choice1) c2 choice2 = if choice1 < choice2
                                    then (c1, choice1)
                                    else (c2, choice2)




-- Started working on MCTS but didn't have time.

-- makeSeed :: [Card] -> Int
-- makeSeed = foldr (flip (flip (^) . (`mod` 1000)) . (+2) . fromEnum . rank) 1

-- data Decision a = Move Int Int a
--   deriving (Eq, Show)

-- type Move = Decision Card

-- printForest :: T.Forest Move -> String
-- printForest = T.drawForest . (fmap . fmap) show

-- newMCST :: [Card] -> T.Forest Move
-- newMCST cs = flip S.evalState cs $ T.unfoldForestM treeMaker (Move 0 0 <$> cs)

-- treeMaker :: Move -> S.State [Card] (Move, [Move])
-- treeMaker m@(Move _ _ c) = do
--   S.modify $ filter (/= c)
--   cs <- S.get
--   return (m, Move 0 0 <$> cs)



----------------------------------------------------------
-- RULES
----------------------------------------------------------

isTrump :: Card -> Bool
isTrump c = case c of
  (Card Heart _)     -> True
  (Card Spade Queen) -> True
  _                  -> False

type Rule = Game [Card]

-- | The first 'Rule' which does not contain Nothing is returned.
firstValidRule :: [Rule] -> Rule
firstValidRule = foldl' (<|>) (lift Nothing)

-- | Return the intersection of the hands generated by each rule.
overlapRules :: [Rule] -> Rule
overlapRules = foldl' ((<*>) . fmap intersect) (asks hand)

-- | Apply all known rules to the hand and return any cards which
-- can be played. Makes no decision about which card would be best.
validCards :: Game [Card]
validCards =
  firstValidRule [

    allPointsOnRoundOne,
    twoClubsOnRoundOne,

    overlapRules [
        noPointsOnRoundOne,
        noHeartsWithoutBreak,
        followSuit
        ]
    ]

isGameStart :: MyGameState -> Bool
isGameStart = (==0) . turn

ignoreRule :: Game a
ignoreRule = lift Nothing

allPointsOnRoundOne :: Game [Card]
allPointsOnRoundOne = do
  isFirstTrick <- asks $ isGameStart . state
  allHearts <- asks $ all isTrump . hand
  if allHearts && isFirstTrick
    then asks hand
    else ignoreRule

twoClubsOnRoundOne :: Game [Card]
twoClubsOnRoundOne = do
  firstToPlay <- asks $ null . trick
  isFirstTrick <- asks $ isGameStart . state
  hasTwoClubs <- asks $ elem (Card Club Two) . hand

  if firstToPlay && isFirstTrick && hasTwoClubs
    then return [Card Club Two]
    else ignoreRule

noPointsOnRoundOne :: Game [Card]
noPointsOnRoundOne = do
  isFirstTrick <- asks $ isGameStart . state
  myHand <- asks hand

  return $ if isFirstTrick
    then filter (not . isTrump) myHand
    else myHand


noHeartsWithoutBreak :: Game [Card]
noHeartsWithoutBreak = do
  hasBroken <- asks $ broken . state
  noHearts <- asks $ filter (not . isHeart) . hand

  if hasBroken
    then asks hand
    else return noHearts

leadingSuit :: Trick -> Maybe Suit
leadingSuit = fmap suit . tailMay . fmap fst

followSuit :: Game [Card]
followSuit = do
  leadSuit <- asks $ leadingSuit . trick
  myhand <- asks hand

  let matchingCards = do
        s <- leadSuit
        return $ filter ((==s) . suit) myhand

  let isNonZeroMatchingCards = maybe False (not . null) matchingCards

  if isNonZeroMatchingCards
    then lift matchingCards
    else asks hand


maxValidCard :: [Card] -> Maybe Card
maxValidCard [] = Nothing
maxValidCard cs = bestWhile ((==(suit $ head cs)) . suit) cs

idealCard :: Suit -> [Card] -> [Card] -> Maybe Card
idealCard s tr ha =
  let currBest = highestInSuit s tr
  in join $ bestInSuitUnderMax <$> currBest <*> pure ha

-- lowestCard :: Suit -> [Card] -> Card
-- lowestCard c1 c2

bestWhile :: (Ord a) => (a -> Bool) -> [a] -> Maybe a
bestWhile = (maximumMay .) . filter

trickWinner :: Trick -> Maybe Card
trickWinner t = do
  s <- trickSuit t
  highestInSuit s (fst <$> t)

trickSuit :: Trick -> Maybe Suit
trickSuit = fmap suit . headMay . fmap fst

maximumMay :: (Ord a) => [a] -> Maybe a
maximumMay [] = Nothing
maximumMay xs = pure $ maximum xs

minimumMay :: (Ord a) => [a] -> Maybe a
minimumMay [] = Nothing
minimumMay xs = pure $ minimum xs

headMay :: [a] -> Maybe a
headMay []    = Nothing
headMay (x:_) = Just x

tailMay :: [a] -> Maybe a
tailMay []     = Nothing
tailMay [x]    = Just x
tailMay (_:xs) = tailMay xs

highestInSuit :: Suit -> [Card] -> Maybe Card
highestInSuit s = bestInSuitUnderMax (Card s maxBound)

bestInSuitUnderMax :: Card -> [Card] -> Maybe Card
bestInSuitUnderMax c = bestWhile (\x -> suit x == suit c && x <= c)

-- | Not used, do not remove.
makeBid :: BidFunc
makeBid = undefined
